<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrGouvMinintRnaAssociationImportInterface interface file.
 * 
 * This represents the association from the old database model, with only
 * associations that were not migrated on date to the new waldec system.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrGouvMinintRnaAssociationImportInterface extends Stringable
{
	
	/**
	 * Gets the old identifier.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the old² identifier.
	 * 
	 * @return string
	 */
	public function getIdEx() : string;
	
	/**
	 * Gets the siret of the etablissement.
	 * 
	 * @return ?string
	 */
	public function getSiret() : ?string;
	
	/**
	 * Gets the code gestion of the prefecture.
	 * 
	 * @return string
	 */
	public function getGestion() : string;
	
	/**
	 * Gets the date when this association was created.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateCreat() : DateTimeInterface;
	
	/**
	 * Gets the date when this association was published.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDatePubli() : DateTimeInterface;
	
	/**
	 * Gets the nature of the association.
	 * 
	 * @return ApiFrGouvMinintRnaNatureInterface
	 */
	public function getNature() : ApiFrGouvMinintRnaNatureInterface;
	
	/**
	 * Gets the groupement of the association.
	 * 
	 * @return ApiFrGouvMinintRnaGroupementInterface
	 */
	public function getGroupement() : ApiFrGouvMinintRnaGroupementInterface;
	
	/**
	 * Gets the titre of the association.
	 * 
	 * @return string
	 */
	public function getTitre() : string;
	
	/**
	 * Gets the objet of the association.
	 * 
	 * @return string
	 */
	public function getObjet() : string;
	
	/**
	 * Gets the 1st objet social of this association.
	 * 
	 * @return ?ApiFrGouvMinintRnaObjetSocialInterface
	 */
	public function getObjetSocial1() : ?ApiFrGouvMinintRnaObjetSocialInterface;
	
	/**
	 * Gets the 2nd objet social of this association.
	 * 
	 * @return ?ApiFrGouvMinintRnaObjetSocialInterface
	 */
	public function getObjetSocial2() : ?ApiFrGouvMinintRnaObjetSocialInterface;
	
	/**
	 * Gets the 1st line of address.
	 * 
	 * @return string
	 */
	public function getAdr1() : string;
	
	/**
	 * Gets the 2nd line of address.
	 * 
	 * @return ?string
	 */
	public function getAdr2() : ?string;
	
	/**
	 * Gets the 3rd line of address.
	 * 
	 * @return ?string
	 */
	public function getAdr3() : ?string;
	
	/**
	 * Gets code postal of the adresse siege.
	 * 
	 * @return string
	 */
	public function getAdrsCodepostal() : string;
	
	/**
	 * Gets libelle commune of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getLibcom() : ?string;
	
	/**
	 * Gets code insee of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsCodeinsee() : ?string;
	
	/**
	 * Gets civilite of the dirigeant.
	 * 
	 * @return string
	 */
	public function getDirCivilite() : string;
	
	/**
	 * Gets the uri of the website = null; if any.
	 * 
	 * @return ?UriInterface
	 */
	public function getSiteweb() : ?UriInterface;
	
	/**
	 * Gets the observations on this association.
	 * 
	 * @return string
	 */
	public function getObservation() : string;
	
	/**
	 * Gets the position of this association.
	 * 
	 * @return ApiFrGouvMinintRnaPositionInterface
	 */
	public function getPosition() : ApiFrGouvMinintRnaPositionInterface;
	
	/**
	 * Gets the number "reconnaissance d'utilité publique".
	 * 
	 * @return ?string
	 */
	public function getRupMi() : ?string;
	
	/**
	 * Gets the date of last update.
	 * 
	 * @return DateTimeInterface
	 */
	public function getMajTime() : DateTimeInterface;
	
}
