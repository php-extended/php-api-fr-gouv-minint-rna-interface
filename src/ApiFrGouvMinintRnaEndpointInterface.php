<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use DateTimeInterface;
use Iterator;
use PhpExtended\Reifier\ReifierReportInterface;
use RuntimeException;
use Stringable;

/**
 * ApiFrGouvMinintRnaEndpointInterface interface file.
 * 
 * This interface represents the driver to get bulk files from the minint (which
 * are deposed on data.gouv.fr).
 * 
 * @author Anastaszor
 */
interface ApiFrGouvMinintRnaEndpointInterface extends Stringable
{
	
	/**
	 * Gets the date of last upload for the searched files.
	 *
	 * @return DateTimeInterface
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getLatestUploadDate() : DateTimeInterface;
	
	/**
	 * Gets the iterator for the (old) import associations.
	 * 
	 * If the report is given, then this method will not throw any reification
	 * exception but will fill the report of failed objects/lines.
	 * 
	 * @param ?ReifierReportInterface $report
	 * @return Iterator<integer, ApiFrGouvMinintRnaAssociationImportInterface>
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getLatestRnaImportIterator(?ReifierReportInterface $report = null) : Iterator;
	
	/**
	 * Gets the iterator for the waldec associations.
	 * 
	 * If the report is given, then this method will not throw any reification
	 * exception but will fill the report of failed objects/lines.
	 * 
	 * @param ?ReifierReportInterface $report
	 * @return Iterator<integer, ApiFrGouvMinintRnaAssociationWaldecInterface>
	 * @throws RuntimeException if the document cannot be downloaded
	 */
	public function getLatestRnaWaldecIterator(?ReifierReportInterface $report = null) : Iterator;
	
	/**
	 * Gets all the Natures of associations.
	 * 
	 * @return Iterator<integer, ApiFrGouvMinintRnaNatureInterface>
	 */
	public function getNatureIterator() : Iterator;
	
	/**
	 * Gets all the Groupements.
	 * 
	 * @return Iterator<integer, ApiFrGouvMinintRnaGroupementInterface>
	 */
	public function getGroupementIterator() : Iterator;
	
	/**
	 * Gets all the Objets Sociaux.
	 * 
	 * @return Iterator<integer, ApiFrGouvMinintRnaObjetSocialInterface>
	 */
	public function getObjetSocialIterator() : Iterator;
	
	/**
	 * Gets all the Positions d'Activité de l'Association.
	 * 
	 * @return Iterator<integer, ApiFrGouvMinintRnaPositionInterface>
	 */
	public function getPositionIterator() : Iterator;
	
}
