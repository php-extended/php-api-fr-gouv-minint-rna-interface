<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use Stringable;

/**
 * ApiFrGouvMinintRnaGroupementInterface interface file.
 * 
 * This represents the minint rna nomenclature that was used to label
 * associations' Groupement Status.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvMinintRnaGroupementInterface extends Stringable
{
	
	/**
	 * Gets the code of this groupement.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the sigle of this groupement.
	 * 
	 * @return string
	 */
	public function getCode() : string;
	
	/**
	 * Gets the name of this groupement.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
}
