<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use Stringable;

/**
 * ApiFrGouvMinintRnaObjetSocialInterface interface file.
 * 
 * This represents the minint rna nomenclature that was used to label
 * associations' Objet Social.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvMinintRnaObjetSocialInterface extends Stringable
{
	
	/**
	 * Gets the code of this object.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the ordre social of this object.
	 * 
	 * @return ApiFrGouvMinintRnaOrdreSocialInterface
	 */
	public function getOrdreSocial() : ApiFrGouvMinintRnaOrdreSocialInterface;
	
	/**
	 * Gets the name of this object.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
}
