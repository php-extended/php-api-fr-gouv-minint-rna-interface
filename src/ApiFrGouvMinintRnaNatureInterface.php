<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use Stringable;

/**
 * ApiFrGouvMinintRnaNatureInterface interface file.
 * 
 * This represents the minint rna nomenclature that was used to label
 * associations' Nature.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvMinintRnaNatureInterface extends Stringable
{
	
	/**
	 * Gets the code of this nature.
	 * 
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * Gets the sigle of this nature.
	 * 
	 * @return string
	 */
	public function getCode() : string;
	
	/**
	 * Gets the name of this nature.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
}
