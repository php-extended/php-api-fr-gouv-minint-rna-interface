<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-minint-rna-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvMinintRna;

use DateTimeInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ApiFrGouvMinintRnaAssociationWaldecInterface interface file.
 * 
 * This represents the association from the new database model, with only
 * associations that were migrated on date to the new waldec system.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
interface ApiFrGouvMinintRnaAssociationWaldecInterface extends Stringable
{
	
	/**
	 * Gets the waldec identifier.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the old identifier.
	 * 
	 * @return string
	 */
	public function getIdEx() : string;
	
	/**
	 * Gets the siret of the etablissement.
	 * 
	 * @return ?string
	 */
	public function getSiret() : ?string;
	
	/**
	 * Gets the number "reconnaissance d'utilité publique".
	 * 
	 * @return ?string
	 */
	public function getRupMi() : ?string;
	
	/**
	 * Gets the code gestion of the prefecture.
	 * 
	 * @return string
	 */
	public function getGestion() : string;
	
	/**
	 * Gets the date when this association was created.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateCreat() : DateTimeInterface;
	
	/**
	 * Gets the date when this association was declared.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateDecla() : DateTimeInterface;
	
	/**
	 * Gets the date when this association was published.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDatePubli() : DateTimeInterface;
	
	/**
	 * Gets the date when this association was dissolved.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateDisso() : ?DateTimeInterface;
	
	/**
	 * Gets the nature of the association.
	 * 
	 * @return ApiFrGouvMinintRnaNatureInterface
	 */
	public function getNature() : ApiFrGouvMinintRnaNatureInterface;
	
	/**
	 * Gets the groupement of the association.
	 * 
	 * @return ApiFrGouvMinintRnaGroupementInterface
	 */
	public function getGroupement() : ApiFrGouvMinintRnaGroupementInterface;
	
	/**
	 * Gets the titre of the association.
	 * 
	 * @return string
	 */
	public function getTitre() : string;
	
	/**
	 * Gets the titre court of the association.
	 * 
	 * @return string
	 */
	public function getTitreCourt() : string;
	
	/**
	 * Gets the objet of the association.
	 * 
	 * @return string
	 */
	public function getObjet() : string;
	
	/**
	 * Gets the 1st objet social of this association.
	 * 
	 * @return ?ApiFrGouvMinintRnaObjetSocialInterface
	 */
	public function getObjetSocial1() : ?ApiFrGouvMinintRnaObjetSocialInterface;
	
	/**
	 * Gets the 2nd objet social of this association.
	 * 
	 * @return ?ApiFrGouvMinintRnaObjetSocialInterface
	 */
	public function getObjetSocial2() : ?ApiFrGouvMinintRnaObjetSocialInterface;
	
	/**
	 * Gets complement of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsComplement() : ?string;
	
	/**
	 * Gets num voie of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsNumVoie() : ?string;
	
	/**
	 * Gets repetition of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsRepetition() : ?string;
	
	/**
	 * Gets type voie of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsTypeVoie() : ?string;
	
	/**
	 * Gets libelle voie of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsLibvoie() : ?string;
	
	/**
	 * Gets code Distrib of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsDistrib() : ?string;
	
	/**
	 * Gets code insee of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsCodeinsee() : ?string;
	
	/**
	 * Gets code postal of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsCodepostal() : ?string;
	
	/**
	 * Gets libelle commune of the adresse siege.
	 * 
	 * @return ?string
	 */
	public function getAdrsLibcommune() : ?string;
	
	/**
	 * Gets declarant of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgDeclarant() : ?string;
	
	/**
	 * Gets complement 1 of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgComplemid() : ?string;
	
	/**
	 * Gets complement 2 of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgComplemgeo() : ?string;
	
	/**
	 * Gets libelle voie of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgLibvoie() : ?string;
	
	/**
	 * Gets code Distrib of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgDistrib() : ?string;
	
	/**
	 * Gets code postal of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgCodepostal() : ?string;
	
	/**
	 * Gets acheminement of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgAchemine() : ?string;
	
	/**
	 * Gets country of the adresse gestion.
	 * 
	 * @return ?string
	 */
	public function getAdrgPays() : ?string;
	
	/**
	 * Gets civilite of the dirigeant.
	 * 
	 * @return string
	 */
	public function getDirCivilite() : string;
	
	/**
	 * Gets the uri of the website = null; if any.
	 * 
	 * @return ?UriInterface
	 */
	public function getSiteweb() : ?UriInterface;
	
	/**
	 * Gets true if the association allows publishing on the web.
	 * 
	 * @return bool
	 */
	public function hasPubliweb() : bool;
	
	/**
	 * Gets the observations on this association.
	 * 
	 * @return ?string
	 */
	public function getObservation() : ?string;
	
	/**
	 * Gets the position of this association.
	 * 
	 * @return ApiFrGouvMinintRnaPositionInterface
	 */
	public function getPosition() : ApiFrGouvMinintRnaPositionInterface;
	
	/**
	 * Gets the date of last update.
	 * 
	 * @return DateTimeInterface
	 */
	public function getMajTime() : DateTimeInterface;
	
}
