# php-extended/php-api-fr-gouv-minint-rna-interface

A library that provides RNA (Répertoire National des Associations) data as objects from data.gouv.fr

![coverage](https://gitlab.com/php-extended/php-api-fr-gouv-minint-rna-interface/badges/main/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-api-fr-gouv-minint-rna-interface ^8`


## Basic Usage

This library is an interface-only library.

For a concrete implementation, see [`php-extended/php-api-fr-gouv-minint-rna-object`](https://gitlab.com/php-extended/php-api-fr-gouv-minint-rna-object).


## License

MIT (See [license file](LICENSE)).


